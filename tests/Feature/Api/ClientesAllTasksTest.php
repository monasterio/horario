<?php

namespace Tests\Feature\Api;

use App\Models\User;
use App\Models\Tasks;
use App\Models\Clientes;

use Tests\TestCase;
use Laravel\Sanctum\Sanctum;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ClientesAllTasksTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $user = User::factory()->create(['email' => 'admin@admin.com']);

        Sanctum::actingAs($user, [], 'web');

        $this->seed(\Database\Seeders\PermissionsSeeder::class);

        $this->withoutExceptionHandling();
    }

    /**
     * @test
     */
    public function it_gets_clientes_all_tasks()
    {
        $clientes = Clientes::factory()->create();
        $allTasks = Tasks::factory()
            ->count(2)
            ->create([
                'clientes_id' => $clientes->id,
            ]);

        $response = $this->getJson(
            route('api.all-clientes.all-tasks.index', $clientes)
        );

        $response->assertOk()->assertSee($allTasks[0]->title);
    }

    /**
     * @test
     */
    public function it_stores_the_clientes_all_tasks()
    {
        $clientes = Clientes::factory()->create();
        $data = Tasks::factory()
            ->make([
                'clientes_id' => $clientes->id,
            ])
            ->toArray();

        $response = $this->postJson(
            route('api.all-clientes.all-tasks.store', $clientes),
            $data
        );

        $this->assertDatabaseHas('tasks', $data);

        $response->assertStatus(201)->assertJsonFragment($data);

        $tasks = Tasks::latest('id')->first();

        $this->assertEquals($clientes->id, $tasks->clientes_id);
    }
}
