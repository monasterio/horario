<?php

namespace Tests\Feature\Api;

use App\Models\User;
use App\Models\Tasks;

use App\Models\Works;
use App\Models\Clientes;

use Tests\TestCase;
use Laravel\Sanctum\Sanctum;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class TasksTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $user = User::factory()->create(['email' => 'admin@admin.com']);

        Sanctum::actingAs($user, [], 'web');

        $this->seed(\Database\Seeders\PermissionsSeeder::class);

        $this->withoutExceptionHandling();
    }

    /**
     * @test
     */
    public function it_gets_all_tasks_list()
    {
        $allTasks = Tasks::factory()
            ->count(5)
            ->create();

        $response = $this->getJson(route('api.all-tasks.index'));

        $response->assertOk()->assertSee($allTasks[0]->title);
    }

    /**
     * @test
     */
    public function it_stores_the_tasks()
    {
        $data = Tasks::factory()
            ->make()
            ->toArray();

        $response = $this->postJson(route('api.all-tasks.store'), $data);

        $this->assertDatabaseHas('tasks', $data);

        $response->assertStatus(201)->assertJsonFragment($data);
    }

    /**
     * @test
     */
    public function it_updates_the_tasks()
    {
        $tasks = Tasks::factory()->create();

        $clientes = Clientes::factory()->create();
        $works = Works::factory()->create();

        $data = [
            'clientes_id' => $this->faker->randomNumber,
            'works_id' => $this->faker->randomNumber,
            'title' => $this->faker->sentence(10),
            'description' => $this->faker->sentence(15),
            'start_date' => $this->faker->dateTime('now', 'UTC'),
            'end_date' => $this->faker->dateTime('now', 'UTC'),
            'stimated_hours' => $this->faker->date,
            'real_hours' => $this->faker->randomNumber(2),
            'cost_per_hour' => $this->faker->randomNumber(2),
            'clientes_id' => $clientes->id,
            'works_id' => $works->id,
        ];

        $response = $this->putJson(
            route('api.all-tasks.update', $tasks),
            $data
        );

        $data['id'] = $tasks->id;

        $this->assertDatabaseHas('tasks', $data);

        $response->assertOk()->assertJsonFragment($data);
    }

    /**
     * @test
     */
    public function it_deletes_the_tasks()
    {
        $tasks = Tasks::factory()->create();

        $response = $this->deleteJson(route('api.all-tasks.destroy', $tasks));

        $this->assertSoftDeleted($tasks);

        $response->assertNoContent();
    }
}
