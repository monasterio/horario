<?php

namespace Tests\Feature\Api;

use App\Models\User;
use App\Models\Works;

use App\Models\Clientes;

use Tests\TestCase;
use Laravel\Sanctum\Sanctum;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class WorksTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $user = User::factory()->create(['email' => 'admin@admin.com']);

        Sanctum::actingAs($user, [], 'web');

        $this->seed(\Database\Seeders\PermissionsSeeder::class);

        $this->withoutExceptionHandling();
    }

    /**
     * @test
     */
    public function it_gets_all_works_list()
    {
        $allWorks = Works::factory()
            ->count(5)
            ->create();

        $response = $this->getJson(route('api.all-works.index'));

        $response->assertOk()->assertSee($allWorks[0]->name);
    }

    /**
     * @test
     */
    public function it_stores_the_works()
    {
        $data = Works::factory()
            ->make()
            ->toArray();

        $response = $this->postJson(route('api.all-works.store'), $data);

        $this->assertDatabaseHas('works', $data);

        $response->assertStatus(201)->assertJsonFragment($data);
    }

    /**
     * @test
     */
    public function it_updates_the_works()
    {
        $works = Works::factory()->create();

        $clientes = Clientes::factory()->create();

        $data = [
            'clientes_id' => $this->faker->randomNumber,
            'name' => $this->faker->text(255),
            'description' => $this->faker->sentence(15),
            'deleted' => $this->faker->text,
            'start_date' => $this->faker->dateTime,
            'end_date' => $this->faker->dateTime,
            'clientes_id' => $clientes->id,
        ];

        $response = $this->putJson(
            route('api.all-works.update', $works),
            $data
        );

        $data['id'] = $works->id;

        $this->assertDatabaseHas('works', $data);

        $response->assertOk()->assertJsonFragment($data);
    }

    /**
     * @test
     */
    public function it_deletes_the_works()
    {
        $works = Works::factory()->create();

        $response = $this->deleteJson(route('api.all-works.destroy', $works));

        $this->assertSoftDeleted($works);

        $response->assertNoContent();
    }
}
