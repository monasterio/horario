<?php

namespace Tests\Feature\Api;

use App\Models\User;
use App\Models\Works;
use App\Models\Clientes;

use Tests\TestCase;
use Laravel\Sanctum\Sanctum;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ClientesAllWorksTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $user = User::factory()->create(['email' => 'admin@admin.com']);

        Sanctum::actingAs($user, [], 'web');

        $this->seed(\Database\Seeders\PermissionsSeeder::class);

        $this->withoutExceptionHandling();
    }

    /**
     * @test
     */
    public function it_gets_clientes_all_works()
    {
        $clientes = Clientes::factory()->create();
        $allWorks = Works::factory()
            ->count(2)
            ->create([
                'clientes_id' => $clientes->id,
            ]);

        $response = $this->getJson(
            route('api.all-clientes.all-works.index', $clientes)
        );

        $response->assertOk()->assertSee($allWorks[0]->name);
    }

    /**
     * @test
     */
    public function it_stores_the_clientes_all_works()
    {
        $clientes = Clientes::factory()->create();
        $data = Works::factory()
            ->make([
                'clientes_id' => $clientes->id,
            ])
            ->toArray();

        $response = $this->postJson(
            route('api.all-clientes.all-works.store', $clientes),
            $data
        );

        $this->assertDatabaseHas('works', $data);

        $response->assertStatus(201)->assertJsonFragment($data);

        $works = Works::latest('id')->first();

        $this->assertEquals($clientes->id, $works->clientes_id);
    }
}
