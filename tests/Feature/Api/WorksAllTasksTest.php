<?php

namespace Tests\Feature\Api;

use App\Models\User;
use App\Models\Works;
use App\Models\Tasks;

use Tests\TestCase;
use Laravel\Sanctum\Sanctum;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class WorksAllTasksTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $user = User::factory()->create(['email' => 'admin@admin.com']);

        Sanctum::actingAs($user, [], 'web');

        $this->seed(\Database\Seeders\PermissionsSeeder::class);

        $this->withoutExceptionHandling();
    }

    /**
     * @test
     */
    public function it_gets_works_all_tasks()
    {
        $works = Works::factory()->create();
        $allTasks = Tasks::factory()
            ->count(2)
            ->create([
                'works_id' => $works->id,
            ]);

        $response = $this->getJson(
            route('api.all-works.all-tasks.index', $works)
        );

        $response->assertOk()->assertSee($allTasks[0]->title);
    }

    /**
     * @test
     */
    public function it_stores_the_works_all_tasks()
    {
        $works = Works::factory()->create();
        $data = Tasks::factory()
            ->make([
                'works_id' => $works->id,
            ])
            ->toArray();

        $response = $this->postJson(
            route('api.all-works.all-tasks.store', $works),
            $data
        );

        $this->assertDatabaseHas('tasks', $data);

        $response->assertStatus(201)->assertJsonFragment($data);

        $tasks = Tasks::latest('id')->first();

        $this->assertEquals($works->id, $tasks->works_id);
    }
}
