<?php

namespace Tests\Feature\Api;

use App\Models\User;
use App\Models\Clientes;

use Tests\TestCase;
use Laravel\Sanctum\Sanctum;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ClientesTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $user = User::factory()->create(['email' => 'admin@admin.com']);

        Sanctum::actingAs($user, [], 'web');

        $this->seed(\Database\Seeders\PermissionsSeeder::class);

        $this->withoutExceptionHandling();
    }

    /**
     * @test
     */
    public function it_gets_all_clientes_list()
    {
        $allClientes = Clientes::factory()
            ->count(5)
            ->create();

        $response = $this->getJson(route('api.all-clientes.index'));

        $response->assertOk()->assertSee($allClientes[0]->name);
    }

    /**
     * @test
     */
    public function it_stores_the_clientes()
    {
        $data = Clientes::factory()
            ->make()
            ->toArray();

        $response = $this->postJson(route('api.all-clientes.store'), $data);

        $this->assertDatabaseHas('clientes', $data);

        $response->assertStatus(201)->assertJsonFragment($data);
    }

    /**
     * @test
     */
    public function it_updates_the_clientes()
    {
        $clientes = Clientes::factory()->create();

        $data = [
            'name' => $this->faker->name,
            'description_proyect' => $this->faker->text,
            'registro_ciente' => $this->faker->date,
        ];

        $response = $this->putJson(
            route('api.all-clientes.update', $clientes),
            $data
        );

        $data['id'] = $clientes->id;

        $this->assertDatabaseHas('clientes', $data);

        $response->assertOk()->assertJsonFragment($data);
    }

    /**
     * @test
     */
    public function it_deletes_the_clientes()
    {
        $clientes = Clientes::factory()->create();

        $response = $this->deleteJson(
            route('api.all-clientes.destroy', $clientes)
        );

        $this->assertSoftDeleted($clientes);

        $response->assertNoContent();
    }
}
