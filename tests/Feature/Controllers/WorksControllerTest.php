<?php

namespace Tests\Feature\Controllers;

use App\Models\User;
use App\Models\Works;

use App\Models\Clientes;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class WorksControllerTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $this->actingAs(
            User::factory()->create(['email' => 'admin@admin.com'])
        );

        $this->seed(\Database\Seeders\PermissionsSeeder::class);

        $this->withoutExceptionHandling();
    }

    /**
     * @test
     */
    public function it_displays_index_view_with_all_works()
    {
        $allWorks = Works::factory()
            ->count(5)
            ->create();

        $response = $this->get(route('all-works.index'));

        $response
            ->assertOk()
            ->assertViewIs('app.all_works.index')
            ->assertViewHas('allWorks');
    }

    /**
     * @test
     */
    public function it_displays_create_view_for_works()
    {
        $response = $this->get(route('all-works.create'));

        $response->assertOk()->assertViewIs('app.all_works.create');
    }

    /**
     * @test
     */
    public function it_stores_the_works()
    {
        $data = Works::factory()
            ->make()
            ->toArray();

        $response = $this->post(route('all-works.store'), $data);

        $this->assertDatabaseHas('works', $data);

        $works = Works::latest('id')->first();

        $response->assertRedirect(route('all-works.edit', $works));
    }

    /**
     * @test
     */
    public function it_displays_show_view_for_works()
    {
        $works = Works::factory()->create();

        $response = $this->get(route('all-works.show', $works));

        $response
            ->assertOk()
            ->assertViewIs('app.all_works.show')
            ->assertViewHas('works');
    }

    /**
     * @test
     */
    public function it_displays_edit_view_for_works()
    {
        $works = Works::factory()->create();

        $response = $this->get(route('all-works.edit', $works));

        $response
            ->assertOk()
            ->assertViewIs('app.all_works.edit')
            ->assertViewHas('works');
    }

    /**
     * @test
     */
    public function it_updates_the_works()
    {
        $works = Works::factory()->create();

        $clientes = Clientes::factory()->create();

        $data = [
            'clientes_id' => $this->faker->randomNumber,
            'name' => $this->faker->text(255),
            'description' => $this->faker->sentence(15),
            'deleted' => $this->faker->text,
            'start_date' => $this->faker->dateTime,
            'end_date' => $this->faker->dateTime,
            'clientes_id' => $clientes->id,
        ];

        $response = $this->put(route('all-works.update', $works), $data);

        $data['id'] = $works->id;

        $this->assertDatabaseHas('works', $data);

        $response->assertRedirect(route('all-works.edit', $works));
    }

    /**
     * @test
     */
    public function it_deletes_the_works()
    {
        $works = Works::factory()->create();

        $response = $this->delete(route('all-works.destroy', $works));

        $response->assertRedirect(route('all-works.index'));

        $this->assertSoftDeleted($works);
    }
}
