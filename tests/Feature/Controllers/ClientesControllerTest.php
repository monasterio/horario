<?php

namespace Tests\Feature\Controllers;

use App\Models\User;
use App\Models\Clientes;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ClientesControllerTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $this->actingAs(
            User::factory()->create(['email' => 'admin@admin.com'])
        );

        $this->seed(\Database\Seeders\PermissionsSeeder::class);

        $this->withoutExceptionHandling();
    }

    /**
     * @test
     */
    public function it_displays_index_view_with_all_clientes()
    {
        $allClientes = Clientes::factory()
            ->count(5)
            ->create();

        $response = $this->get(route('all-clientes.index'));

        $response
            ->assertOk()
            ->assertViewIs('app.all_clientes.index')
            ->assertViewHas('allClientes');
    }

    /**
     * @test
     */
    public function it_displays_create_view_for_clientes()
    {
        $response = $this->get(route('all-clientes.create'));

        $response->assertOk()->assertViewIs('app.all_clientes.create');
    }

    /**
     * @test
     */
    public function it_stores_the_clientes()
    {
        $data = Clientes::factory()
            ->make()
            ->toArray();

        $response = $this->post(route('all-clientes.store'), $data);

        $this->assertDatabaseHas('clientes', $data);

        $clientes = Clientes::latest('id')->first();

        $response->assertRedirect(route('all-clientes.edit', $clientes));
    }

    /**
     * @test
     */
    public function it_displays_show_view_for_clientes()
    {
        $clientes = Clientes::factory()->create();

        $response = $this->get(route('all-clientes.show', $clientes));

        $response
            ->assertOk()
            ->assertViewIs('app.all_clientes.show')
            ->assertViewHas('clientes');
    }

    /**
     * @test
     */
    public function it_displays_edit_view_for_clientes()
    {
        $clientes = Clientes::factory()->create();

        $response = $this->get(route('all-clientes.edit', $clientes));

        $response
            ->assertOk()
            ->assertViewIs('app.all_clientes.edit')
            ->assertViewHas('clientes');
    }

    /**
     * @test
     */
    public function it_updates_the_clientes()
    {
        $clientes = Clientes::factory()->create();

        $data = [
            'name' => $this->faker->name,
            'description_proyect' => $this->faker->text,
            'registro_ciente' => $this->faker->date,
        ];

        $response = $this->put(route('all-clientes.update', $clientes), $data);

        $data['id'] = $clientes->id;

        $this->assertDatabaseHas('clientes', $data);

        $response->assertRedirect(route('all-clientes.edit', $clientes));
    }

    /**
     * @test
     */
    public function it_deletes_the_clientes()
    {
        $clientes = Clientes::factory()->create();

        $response = $this->delete(route('all-clientes.destroy', $clientes));

        $response->assertRedirect(route('all-clientes.index'));

        $this->assertSoftDeleted($clientes);
    }
}
