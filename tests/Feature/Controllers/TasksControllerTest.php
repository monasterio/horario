<?php

namespace Tests\Feature\Controllers;

use App\Models\User;
use App\Models\Tasks;

use App\Models\Works;
use App\Models\Clientes;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class TasksControllerTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $this->actingAs(
            User::factory()->create(['email' => 'admin@admin.com'])
        );

        $this->seed(\Database\Seeders\PermissionsSeeder::class);

        $this->withoutExceptionHandling();
    }

    /**
     * @test
     */
    public function it_displays_index_view_with_all_tasks()
    {
        $allTasks = Tasks::factory()
            ->count(5)
            ->create();

        $response = $this->get(route('all-tasks.index'));

        $response
            ->assertOk()
            ->assertViewIs('app.all_tasks.index')
            ->assertViewHas('allTasks');
    }

    /**
     * @test
     */
    public function it_displays_create_view_for_tasks()
    {
        $response = $this->get(route('all-tasks.create'));

        $response->assertOk()->assertViewIs('app.all_tasks.create');
    }

    /**
     * @test
     */
    public function it_stores_the_tasks()
    {
        $data = Tasks::factory()
            ->make()
            ->toArray();

        $response = $this->post(route('all-tasks.store'), $data);

        $this->assertDatabaseHas('tasks', $data);

        $tasks = Tasks::latest('id')->first();

        $response->assertRedirect(route('all-tasks.edit', $tasks));
    }

    /**
     * @test
     */
    public function it_displays_show_view_for_tasks()
    {
        $tasks = Tasks::factory()->create();

        $response = $this->get(route('all-tasks.show', $tasks));

        $response
            ->assertOk()
            ->assertViewIs('app.all_tasks.show')
            ->assertViewHas('tasks');
    }

    /**
     * @test
     */
    public function it_displays_edit_view_for_tasks()
    {
        $tasks = Tasks::factory()->create();

        $response = $this->get(route('all-tasks.edit', $tasks));

        $response
            ->assertOk()
            ->assertViewIs('app.all_tasks.edit')
            ->assertViewHas('tasks');
    }

    /**
     * @test
     */
    public function it_updates_the_tasks()
    {
        $tasks = Tasks::factory()->create();

        $clientes = Clientes::factory()->create();
        $works = Works::factory()->create();

        $data = [
            'clientes_id' => $this->faker->randomNumber,
            'works_id' => $this->faker->randomNumber,
            'title' => $this->faker->sentence(10),
            'description' => $this->faker->sentence(15),
            'start_date' => $this->faker->dateTime('now', 'UTC'),
            'end_date' => $this->faker->dateTime('now', 'UTC'),
            'stimated_hours' => $this->faker->date,
            'real_hours' => $this->faker->randomNumber(2),
            'cost_per_hour' => $this->faker->randomNumber(2),
            'clientes_id' => $clientes->id,
            'works_id' => $works->id,
        ];

        $response = $this->put(route('all-tasks.update', $tasks), $data);

        $data['id'] = $tasks->id;

        $this->assertDatabaseHas('tasks', $data);

        $response->assertRedirect(route('all-tasks.edit', $tasks));
    }

    /**
     * @test
     */
    public function it_deletes_the_tasks()
    {
        $tasks = Tasks::factory()->create();

        $response = $this->delete(route('all-tasks.destroy', $tasks));

        $response->assertRedirect(route('all-tasks.index'));

        $this->assertSoftDeleted($tasks);
    }
}
