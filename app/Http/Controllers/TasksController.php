<?php

namespace App\Http\Controllers;

use App\Http\Requests\TasksStoreRequest;
use App\Http\Requests\TasksUpdateRequest;
use App\Models\Clientes;
use App\Models\Tasks;
use App\Models\Works;
use Illuminate\Http\Request;

class TasksController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->authorize('view-any', Tasks::class);

        $search = $request->get('search', '');

        $allTasks = Tasks::search($search)
            ->latest()
            ->paginate(5);

        return view('app.all_tasks.index', compact('allTasks', 'search'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $this->authorize('create', Tasks::class);

        $allClientes = Clientes::pluck('name', 'id');
        $allWorks = Works::pluck('name', 'id');

        return view('app.all_tasks.create', compact('allClientes', 'allWorks'));
    }

    /**
     * @param \App\Http\Requests\TasksStoreRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(TasksStoreRequest $request)
    {
        $this->authorize('create', Tasks::class);

        $validated = $request->validated();

        $tasks = Tasks::create($validated);

        return redirect()
            ->route('all-tasks.edit', $tasks)
            ->withSuccess(__('crud.common.created'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Tasks $tasks
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, Tasks $tasks)
    {
        $this->authorize('view', $tasks);

        return view('app.all_tasks.show', compact('tasks'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Tasks $tasks
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, Tasks $tasks)
    {
        $this->authorize('update', $tasks);

        $allClientes = Clientes::pluck('name', 'id');
        $allWorks = Works::pluck('name', 'id');

        return view(
            'app.all_tasks.edit',
            compact('tasks', 'allClientes', 'allWorks')
        );
    }

    /**
     * @param \App\Http\Requests\TasksUpdateRequest $request
     * @param \App\Models\Tasks $tasks
     * @return \Illuminate\Http\Response
     */
    public function update(TasksUpdateRequest $request, Tasks $tasks)
    {
        $this->authorize('update', $tasks);

        $validated = $request->validated();

        $tasks->update($validated);

        return redirect()
            ->route('all-tasks.edit', $tasks)
            ->withSuccess(__('crud.common.saved'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Tasks $tasks
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Tasks $tasks)
    {
        $this->authorize('delete', $tasks);

        $tasks->delete();

        return redirect()
            ->route('all-tasks.index')
            ->withSuccess(__('crud.common.removed'));
    }
}
