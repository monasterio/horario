<?php

namespace App\Http\Controllers;

use App\Models\Works;
use App\Models\Clientes;
use Illuminate\Http\Request;
use App\Http\Requests\WorksStoreRequest;
use App\Http\Requests\WorksUpdateRequest;

class WorksController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->authorize('view-any', Works::class);

        $search = $request->get('search', '');

        $allWorks = Works::search($search)
            ->latest()
            ->paginate(5);

        return view('app.all_works.index', compact('allWorks', 'search'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $this->authorize('create', Works::class);

        $allClientes = Clientes::pluck('name', 'id');

        return view('app.all_works.create', compact('allClientes'));
    }

    /**
     * @param \App\Http\Requests\WorksStoreRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(WorksStoreRequest $request)
    {
        $this->authorize('create', Works::class);

        $validated = $request->validated();

        $works = Works::create($validated);

        return redirect()
            ->route('all-works.edit', $works)
            ->withSuccess(__('crud.common.created'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Works $works
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, Works $works)
    {
        $this->authorize('view', $works);

        return view('app.all_works.show', compact('works'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Works $works
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, Works $works)
    {
        $this->authorize('update', $works);

        $allClientes = Clientes::pluck('name', 'id');

        return view('app.all_works.edit', compact('works', 'allClientes'));
    }

    /**
     * @param \App\Http\Requests\WorksUpdateRequest $request
     * @param \App\Models\Works $works
     * @return \Illuminate\Http\Response
     */
    public function update(WorksUpdateRequest $request, Works $works)
    {
        $this->authorize('update', $works);

        $validated = $request->validated();

        $works->update($validated);

        return redirect()
            ->route('all-works.edit', $works)
            ->withSuccess(__('crud.common.saved'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Works $works
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Works $works)
    {
        $this->authorize('delete', $works);

        $works->delete();

        return redirect()
            ->route('all-works.index')
            ->withSuccess(__('crud.common.removed'));
    }
}
