<?php

namespace App\Http\Controllers\Api;

use App\Models\Clientes;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\TasksResource;
use App\Http\Resources\TasksCollection;

class ClientesAllTasksController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Clientes $clientes
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, Clientes $clientes)
    {
        $this->authorize('view', $clientes);

        $search = $request->get('search', '');

        $allTasks = $clientes
            ->allTasks()
            ->search($search)
            ->latest()
            ->paginate();

        return new TasksCollection($allTasks);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Clientes $clientes
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Clientes $clientes)
    {
        $this->authorize('create', Tasks::class);

        $validated = $request->validate([
            'works_id' => ['nullable', 'exists:works,id'],
            'title' => ['required', 'max:255', 'string'],
            'start_date' => ['nullable', 'date'],
            'end_date' => ['nullable', 'date'],
            'description' => ['required', 'max:255', 'string'],
            'stimated_hours' => ['required', 'date'],
            'real_hours' => ['required', 'numeric'],
            'cost_per_hour' => ['required', 'numeric'],
        ]);

        $tasks = $clientes->allTasks()->create($validated);

        return new TasksResource($tasks);
    }
}
