<?php

namespace App\Http\Controllers\Api;

use App\Models\Clientes;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\ClientesResource;
use App\Http\Resources\ClientesCollection;
use App\Http\Requests\ClientesStoreRequest;
use App\Http\Requests\ClientesUpdateRequest;

class ClientesController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->authorize('view-any', Clientes::class);

        $search = $request->get('search', '');

        $allClientes = Clientes::search($search)
            ->latest()
            ->paginate();

        return new ClientesCollection($allClientes);
    }

    /**
     * @param \App\Http\Requests\ClientesStoreRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(ClientesStoreRequest $request)
    {
        $this->authorize('create', Clientes::class);

        $validated = $request->validated();

        $clientes = Clientes::create($validated);

        return new ClientesResource($clientes);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Clientes $clientes
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, Clientes $clientes)
    {
        $this->authorize('view', $clientes);

        return new ClientesResource($clientes);
    }

    /**
     * @param \App\Http\Requests\ClientesUpdateRequest $request
     * @param \App\Models\Clientes $clientes
     * @return \Illuminate\Http\Response
     */
    public function update(ClientesUpdateRequest $request, Clientes $clientes)
    {
        $this->authorize('update', $clientes);

        $validated = $request->validated();

        $clientes->update($validated);

        return new ClientesResource($clientes);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Clientes $clientes
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Clientes $clientes)
    {
        $this->authorize('delete', $clientes);

        $clientes->delete();

        return response()->noContent();
    }
}
