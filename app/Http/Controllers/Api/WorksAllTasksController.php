<?php

namespace App\Http\Controllers\Api;

use App\Models\Works;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\TasksResource;
use App\Http\Resources\TasksCollection;

class WorksAllTasksController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Works $works
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, Works $works)
    {
        $this->authorize('view', $works);

        $search = $request->get('search', '');

        $allTasks = $works
            ->allTasks()
            ->search($search)
            ->latest()
            ->paginate();

        return new TasksCollection($allTasks);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Works $works
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Works $works)
    {
        $this->authorize('create', Tasks::class);

        $validated = $request->validate([
            'clientes_id' => ['nullable', 'exists:clientes,id'],
            'title' => ['required', 'max:255', 'string'],
            'start_date' => ['nullable', 'date'],
            'end_date' => ['nullable', 'date'],
            'description' => ['required', 'max:255', 'string'],
            'stimated_hours' => ['required', 'date'],
            'real_hours' => ['required', 'numeric'],
            'cost_per_hour' => ['required', 'numeric'],
        ]);

        $tasks = $works->allTasks()->create($validated);

        return new TasksResource($tasks);
    }
}
