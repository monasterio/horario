<?php

namespace App\Http\Controllers\Api;

use App\Models\Tasks;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\TasksResource;
use App\Http\Resources\TasksCollection;
use App\Http\Requests\TasksStoreRequest;
use App\Http\Requests\TasksUpdateRequest;

class TasksController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->authorize('view-any', Tasks::class);

        $search = $request->get('search', '');

        $allTasks = Tasks::search($search)
            ->latest()
            ->paginate();

        return new TasksCollection($allTasks);
    }

    /**
     * @param \App\Http\Requests\TasksStoreRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(TasksStoreRequest $request)
    {
        $this->authorize('create', Tasks::class);

        $validated = $request->validated();

        $tasks = Tasks::create($validated);

        return new TasksResource($tasks);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Tasks $tasks
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, Tasks $tasks)
    {
        $this->authorize('view', $tasks);

        return new TasksResource($tasks);
    }

    /**
     * @param \App\Http\Requests\TasksUpdateRequest $request
     * @param \App\Models\Tasks $tasks
     * @return \Illuminate\Http\Response
     */
    public function update(TasksUpdateRequest $request, Tasks $tasks)
    {
        $this->authorize('update', $tasks);

        $validated = $request->validated();

        $tasks->update($validated);

        return new TasksResource($tasks);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Tasks $tasks
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Tasks $tasks)
    {
        $this->authorize('delete', $tasks);

        $tasks->delete();

        return response()->noContent();
    }
}
