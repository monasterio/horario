<?php

namespace App\Http\Controllers\Api;

use App\Models\Works;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\WorksResource;
use App\Http\Resources\WorksCollection;
use App\Http\Requests\WorksStoreRequest;
use App\Http\Requests\WorksUpdateRequest;

class WorksController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->authorize('view-any', Works::class);

        $search = $request->get('search', '');

        $allWorks = Works::search($search)
            ->latest()
            ->paginate();

        return new WorksCollection($allWorks);
    }

    /**
     * @param \App\Http\Requests\WorksStoreRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(WorksStoreRequest $request)
    {
        $this->authorize('create', Works::class);

        $validated = $request->validated();

        $works = Works::create($validated);

        return new WorksResource($works);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Works $works
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, Works $works)
    {
        $this->authorize('view', $works);

        return new WorksResource($works);
    }

    /**
     * @param \App\Http\Requests\WorksUpdateRequest $request
     * @param \App\Models\Works $works
     * @return \Illuminate\Http\Response
     */
    public function update(WorksUpdateRequest $request, Works $works)
    {
        $this->authorize('update', $works);

        $validated = $request->validated();

        $works->update($validated);

        return new WorksResource($works);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Works $works
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Works $works)
    {
        $this->authorize('delete', $works);

        $works->delete();

        return response()->noContent();
    }
}
