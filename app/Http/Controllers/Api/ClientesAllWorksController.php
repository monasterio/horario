<?php

namespace App\Http\Controllers\Api;

use App\Models\Clientes;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\WorksResource;
use App\Http\Resources\WorksCollection;

class ClientesAllWorksController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Clientes $clientes
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, Clientes $clientes)
    {
        $this->authorize('view', $clientes);

        $search = $request->get('search', '');

        $allWorks = $clientes
            ->allWorks()
            ->search($search)
            ->latest()
            ->paginate();

        return new WorksCollection($allWorks);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Clientes $clientes
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Clientes $clientes)
    {
        $this->authorize('create', Works::class);

        $validated = $request->validate([
            'name' => ['required', 'max:255', 'string'],
            'start_date' => ['required', 'date'],
            'end_date' => ['required', 'date'],
            'deleted' => ['nullable', 'max:255', 'string'],
            'description' => ['required', 'max:255', 'string'],
        ]);

        $works = $clientes->allWorks()->create($validated);

        return new WorksResource($works);
    }
}
