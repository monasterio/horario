<?php

namespace App\Http\Controllers;

use App\Models\Clientes;
use Illuminate\Http\Request;
use App\Http\Requests\ClientesStoreRequest;
use App\Http\Requests\ClientesUpdateRequest;

class ClientesController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->authorize('view-any', Clientes::class);

        $search = $request->get('search', '');

        $allClientes = Clientes::search($search)
            ->latest()
            ->paginate(5);

        return view('app.all_clientes.index', compact('allClientes', 'search'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $this->authorize('create', Clientes::class);

        return view('app.all_clientes.create');
    }

    /**
     * @param \App\Http\Requests\ClientesStoreRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(ClientesStoreRequest $request)
    {
        $this->authorize('create', Clientes::class);

        $validated = $request->validated();

        $clientes = Clientes::create($validated);

        return redirect()
            ->route('all-clientes.edit', $clientes)
            ->withSuccess(__('crud.common.created'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Clientes $clientes
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, Clientes $clientes)
    {
        $this->authorize('view', $clientes);

        return view('app.all_clientes.show', compact('clientes'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Clientes $clientes
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, Clientes $clientes)
    {
        $this->authorize('update', $clientes);

        return view('app.all_clientes.edit', compact('clientes'));
    }

    /**
     * @param \App\Http\Requests\ClientesUpdateRequest $request
     * @param \App\Models\Clientes $clientes
     * @return \Illuminate\Http\Response
     */
    public function update(ClientesUpdateRequest $request, Clientes $clientes)
    {
        $this->authorize('update', $clientes);

        $validated = $request->validated();

        $clientes->update($validated);

        return redirect()
            ->route('all-clientes.edit', $clientes)
            ->withSuccess(__('crud.common.saved'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Clientes $clientes
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Clientes $clientes)
    {
        $this->authorize('delete', $clientes);

        $clientes->delete();

        return redirect()
            ->route('all-clientes.index')
            ->withSuccess(__('crud.common.removed'));
    }
}
