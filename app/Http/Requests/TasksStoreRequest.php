<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TasksStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'clientes_id' => ['nullable', 'exists:clientes,id'],
            'works_id' => ['nullable', 'exists:works,id'],
            'title' => ['required', 'max:255', 'string'],
            'start_date' => ['nullable', 'date'],
            'end_date' => ['nullable', 'date', 'after:start_date'],

            'description' => ['required', 'max:255', 'string'],
            'stimated_hours' => ['required', 'string'],
            'real_hours' => ['required', 'numeric'],
            'cost_per_hour' => ['required', 'numeric'],
        ];
    }
}
