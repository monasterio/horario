<?php

namespace App\Models;

use App\Models\Scopes\Searchable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Clientes extends Model
{
    use SoftDeletes;
    use HasFactory;
    use Searchable;

    protected $fillable = ['name', 'description_proyect', 'registro_ciente'];

    protected $searchableFields = ['*'];

    protected $casts = [
        'registro_ciente' => 'date',
    ];

    public function allWorks()
    {
        return $this->hasMany(Works::class);
    }

    public function allTasks()
    {
        return $this->hasMany(Tasks::class);
    }
}
