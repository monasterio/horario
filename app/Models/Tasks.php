<?php

namespace App\Models;

use App\Models\Scopes\Searchable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Tasks extends Model
{
    use SoftDeletes;
    use HasFactory;
    use Searchable;

    protected $fillable = [
        'clientes_id',
        'works_id',
        'title',
        'description',
        'start_date',
        'end_date',
        'stimated_hours',
        'real_hours',
        'cost_per_hour',
    ];

    protected $searchableFields = ['*'];

    protected $casts = [
        'start_date' => 'datetime',
        'end_date' => 'datetime',

    ];

    public function clientes()
    {
        return $this->belongsTo(Clientes::class);
    }

    public function works()
    {
        return $this->belongsTo(Works::class);
    }
}
