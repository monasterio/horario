<?php

namespace App\Models;

use App\Models\Scopes\Searchable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Works extends Model
{
    use SoftDeletes;
    use HasFactory;
    use Searchable;

    protected $fillable = [
        'clientes_id',
        'name',
        'description',
        'deleted',
        'start_date',
        'end_date',
    ];

    protected $searchableFields = ['*'];

    protected $casts = [
        'start_date' => 'datetime',
        'end_date' => 'datetime',
    ];

    public function clientes()
    {
        return $this->belongsTo(Clientes::class);
    }

    public function allTasks()
    {
        return $this->hasMany(Tasks::class);
    }
}
