<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Works;
use Illuminate\Auth\Access\HandlesAuthorization;

class WorksPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the works can view any models.
     *
     * @param  App\Models\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return $user->hasPermissionTo('list allworks');
    }

    /**
     * Determine whether the works can view the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\Works  $model
     * @return mixed
     */
    public function view(User $user, Works $model)
    {
        return $user->hasPermissionTo('view allworks');
    }

    /**
     * Determine whether the works can create models.
     *
     * @param  App\Models\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->hasPermissionTo('create allworks');
    }

    /**
     * Determine whether the works can update the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\Works  $model
     * @return mixed
     */
    public function update(User $user, Works $model)
    {
        return $user->hasPermissionTo('update allworks');
    }

    /**
     * Determine whether the works can delete the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\Works  $model
     * @return mixed
     */
    public function delete(User $user, Works $model)
    {
        return $user->hasPermissionTo('delete allworks');
    }

    /**
     * Determine whether the user can delete multiple instances of the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\Works  $model
     * @return mixed
     */
    public function deleteAny(User $user)
    {
        return $user->hasPermissionTo('delete allworks');
    }

    /**
     * Determine whether the works can restore the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\Works  $model
     * @return mixed
     */
    public function restore(User $user, Works $model)
    {
        return false;
    }

    /**
     * Determine whether the works can permanently delete the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\Works  $model
     * @return mixed
     */
    public function forceDelete(User $user, Works $model)
    {
        return false;
    }
}
