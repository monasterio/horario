<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Clientes;
use Illuminate\Auth\Access\HandlesAuthorization;

class ClientesPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the clientes can view any models.
     *
     * @param  App\Models\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return $user->hasPermissionTo('list allclientes');
    }

    /**
     * Determine whether the clientes can view the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\Clientes  $model
     * @return mixed
     */
    public function view(User $user, Clientes $model)
    {
        return $user->hasPermissionTo('view allclientes');
    }

    /**
     * Determine whether the clientes can create models.
     *
     * @param  App\Models\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->hasPermissionTo('create allclientes');
    }

    /**
     * Determine whether the clientes can update the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\Clientes  $model
     * @return mixed
     */
    public function update(User $user, Clientes $model)
    {
        return $user->hasPermissionTo('update allclientes');
    }

    /**
     * Determine whether the clientes can delete the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\Clientes  $model
     * @return mixed
     */
    public function delete(User $user, Clientes $model)
    {
        return $user->hasPermissionTo('delete allclientes');
    }

    /**
     * Determine whether the user can delete multiple instances of the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\Clientes  $model
     * @return mixed
     */
    public function deleteAny(User $user)
    {
        return $user->hasPermissionTo('delete allclientes');
    }

    /**
     * Determine whether the clientes can restore the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\Clientes  $model
     * @return mixed
     */
    public function restore(User $user, Clientes $model)
    {
        return false;
    }

    /**
     * Determine whether the clientes can permanently delete the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\Clientes  $model
     * @return mixed
     */
    public function forceDelete(User $user, Clientes $model)
    {
        return false;
    }
}
