<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Tasks;
use Illuminate\Auth\Access\HandlesAuthorization;

class TasksPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the tasks can view any models.
     *
     * @param  App\Models\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return $user->hasPermissionTo('list alltasks');
    }

    /**
     * Determine whether the tasks can view the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\Tasks  $model
     * @return mixed
     */
    public function view(User $user, Tasks $model)
    {
        return $user->hasPermissionTo('view alltasks');
    }

    /**
     * Determine whether the tasks can create models.
     *
     * @param  App\Models\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->hasPermissionTo('create alltasks');
    }

    /**
     * Determine whether the tasks can update the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\Tasks  $model
     * @return mixed
     */
    public function update(User $user, Tasks $model)
    {
        return $user->hasPermissionTo('update alltasks');
    }

    /**
     * Determine whether the tasks can delete the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\Tasks  $model
     * @return mixed
     */
    public function delete(User $user, Tasks $model)
    {
        return $user->hasPermissionTo('delete alltasks');
    }

    /**
     * Determine whether the user can delete multiple instances of the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\Tasks  $model
     * @return mixed
     */
    public function deleteAny(User $user)
    {
        return $user->hasPermissionTo('delete alltasks');
    }

    /**
     * Determine whether the tasks can restore the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\Tasks  $model
     * @return mixed
     */
    public function restore(User $user, Tasks $model)
    {
        return false;
    }

    /**
     * Determine whether the tasks can permanently delete the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\Tasks  $model
     * @return mixed
     */
    public function forceDelete(User $user, Tasks $model)
    {
        return false;
    }
}
