<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\UserController;
use App\Http\Controllers\Api\AuthController;
use App\Http\Controllers\Api\RoleController;
use App\Http\Controllers\Api\WorksController;
use App\Http\Controllers\Api\TasksController;
use App\Http\Controllers\Api\ClientesController;
use App\Http\Controllers\Api\PermissionController;
use App\Http\Controllers\Api\WorksAllTasksController;
use App\Http\Controllers\Api\ClientesAllWorksController;
use App\Http\Controllers\Api\ClientesAllTasksController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/login', [AuthController::class, 'login'])->name('api.login');

Route::middleware('auth:sanctum')
    ->get('/user', function (Request $request) {
        return $request->user();
    })
    ->name('api.user');

Route::name('api.')
    ->middleware('auth:sanctum')
    ->group(function () {
        Route::apiResource('roles', RoleController::class);
        Route::apiResource('permissions', PermissionController::class);

        Route::apiResource('users', UserController::class);

        Route::apiResource('all-clientes', ClientesController::class);

        // Clientes All Works
        Route::get('/all-clientes/{clientes}/all-works', [
            ClientesAllWorksController::class,
            'index',
        ])->name('all-clientes.all-works.index');
        Route::post('/all-clientes/{clientes}/all-works', [
            ClientesAllWorksController::class,
            'store',
        ])->name('all-clientes.all-works.store');

        // Clientes All Tasks
        Route::get('/all-clientes/{clientes}/all-tasks', [
            ClientesAllTasksController::class,
            'index',
        ])->name('all-clientes.all-tasks.index');
        Route::post('/all-clientes/{clientes}/all-tasks', [
            ClientesAllTasksController::class,
            'store',
        ])->name('all-clientes.all-tasks.store');

        Route::apiResource('all-works', WorksController::class);

        // Works All Tasks
        Route::get('/all-works/{works}/all-tasks', [
            WorksAllTasksController::class,
            'index',
        ])->name('all-works.all-tasks.index');
        Route::post('/all-works/{works}/all-tasks', [
            WorksAllTasksController::class,
            'store',
        ])->name('all-works.all-tasks.store');

        Route::apiResource('all-tasks', TasksController::class);
    });
