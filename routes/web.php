<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\WorksController;
use App\Http\Controllers\TasksController;
use App\Http\Controllers\ClientesController;
use App\Http\Controllers\PermissionController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', [HomeController::class, 'index'])->name('home');

Route::prefix('/')
    ->middleware('auth')
    ->group(function () {
        Route::resource('roles', RoleController::class);
        Route::resource('permissions', PermissionController::class);

        Route::resource('users', UserController::class);
        Route::get('all-clientes', [ClientesController::class, 'index'])->name(
            'all-clientes.index'
        );
        Route::post('all-clientes', [ClientesController::class, 'store'])->name(
            'all-clientes.store'
        );
        Route::get('all-clientes/create', [
            ClientesController::class,
            'create',
        ])->name('all-clientes.create');
        Route::get('all-clientes/{clientes}', [
            ClientesController::class,
            'show',
        ])->name('all-clientes.show');
        Route::get('all-clientes/{clientes}/edit', [
            ClientesController::class,
            'edit',
        ])->name('all-clientes.edit');
        Route::put('all-clientes/{clientes}', [
            ClientesController::class,
            'update',
        ])->name('all-clientes.update');
        Route::delete('all-clientes/{clientes}', [
            ClientesController::class,
            'destroy',
        ])->name('all-clientes.destroy');

        Route::get('all-works', [WorksController::class, 'index'])->name(
            'all-works.index'
        );
        Route::post('all-works', [WorksController::class, 'store'])->name(
            'all-works.store'
        );
        Route::get('all-works/create', [
            WorksController::class,
            'create',
        ])->name('all-works.create');
        Route::get('all-works/{works}', [WorksController::class, 'show'])->name(
            'all-works.show'
        );
        Route::get('all-works/{works}/edit', [
            WorksController::class,
            'edit',
        ])->name('all-works.edit');
        Route::put('all-works/{works}', [
            WorksController::class,
            'update',
        ])->name('all-works.update');
        Route::delete('all-works/{works}', [
            WorksController::class,
            'destroy',
        ])->name('all-works.destroy');

        Route::get('all-tasks', [TasksController::class, 'index'])->name(
            'all-tasks.index'
        );
        Route::post('all-tasks', [TasksController::class, 'store'])->name(
            'all-tasks.store'
        );
        Route::get('all-tasks/create', [
            TasksController::class,
            'create',
        ])->name('all-tasks.create');
        Route::get('all-tasks/{tasks}', [TasksController::class, 'show'])->name(
            'all-tasks.show'
        );
        Route::get('all-tasks/{tasks}/edit', [
            TasksController::class,
            'edit',
        ])->name('all-tasks.edit');
        Route::put('all-tasks/{tasks}', [
            TasksController::class,
            'update',
        ])->name('all-tasks.update');
        Route::delete('all-tasks/{tasks}', [
            TasksController::class,
            'destroy',
        ])->name('all-tasks.destroy');
    });
