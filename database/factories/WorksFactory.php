<?php

namespace Database\Factories;

use App\Models\Works;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Factories\Factory;

class WorksFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Works::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->text(255),
            'description' => $this->faker->sentence(15),
            'deleted' => $this->faker->text,
            'start_date' => $this->faker->dateTime,
            'end_date' => $this->faker->dateTime,
            'clientes_id' => \App\Models\Clientes::factory(),
        ];
    }
}
