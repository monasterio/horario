<?php

namespace Database\Factories;

use App\Models\Clientes;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Factories\Factory;

class ClientesFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Clientes::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->name,
            'description_proyect' => $this->faker->text,
            'registro_ciente' => $this->faker->date,
        ];
    }
}
