<?php

namespace Database\Factories;

use App\Models\Tasks;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Factories\Factory;

class TasksFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Tasks::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'title' => $this->faker->sentence(10),
            'description' => $this->faker->sentence(15),
            'start_date' => $this->faker->dateTime('now', 'UTC'),
            'end_date' => $this->faker->dateTime('now', 'UTC'),
            'stimated_hours' => $this->faker->date,
            'real_hours' => $this->faker->randomNumber(2),
            'cost_per_hour' => $this->faker->randomNumber(2),
            'clientes_id' => \App\Models\Clientes::factory(),
            'works_id' => \App\Models\Works::factory(),
        ];
    }
}
