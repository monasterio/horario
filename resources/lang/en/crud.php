<?php

return [
    'common' => [
        'actions' => 'Actions',
        'create' => 'Create',
        'edit' => 'Edit',
        'update' => 'Update',
        'new' => 'New',
        'cancel' => 'Cancel',
        'save' => 'Save',
        'delete' => 'Delete',
        'delete_selected' => 'Delete selected',
        'search' => 'Search...',
        'back' => 'Back to Index',
        'are_you_sure' => 'Are you sure?',
        'no_items_found' => 'No items found',
        'created' => 'Successfully created',
        'saved' => 'Saved successfully',
        'removed' => 'Successfully removed',
    ],

    'users' => [
        'name' => 'Users',
        'index_title' => 'Users List',
        'new_title' => 'New User',
        'create_title' => 'Create User',
        'edit_title' => 'Edit User',
        'show_title' => 'Show User',
        'inputs' => [
            'name' => 'Name',
            'email' => 'Email',
            'password' => 'Password',
        ],
    ],

    'clientes' => [
        'name' => 'Clientes',
        'index_title' => 'AllClientes List',
        'new_title' => 'New Clientes',
        'create_title' => 'Create Clientes',
        'edit_title' => 'Edit Clientes',
        'show_title' => 'Show Clientes',
        'inputs' => [
            'registro_ciente' => 'Registro Ciente',
            'name' => 'Name',
            'description_proyect' => 'Description Proyect',
        ],
    ],

    'works' => [
        'name' => 'Works',
        'index_title' => 'AllWorks List',
        'new_title' => 'New Works',
        'create_title' => 'Create Works',
        'edit_title' => 'Edit Works',
        'show_title' => 'Show Works',
        'inputs' => [
            'clientes_id' => 'Clientes',
            'name' => 'Name',
            'start_date' => 'Start Date',
            'end_date' => 'End Date',
            'deleted' => 'Deleted',
            'description' => 'Description',
        ],
    ],

    'tasks' => [
        'name' => 'Tasks',
        'index_title' => 'AllTasks List',
        'new_title' => 'New Tasks',
        'create_title' => 'Create Tasks',
        'edit_title' => 'Edit Tasks',
        'show_title' => 'Show Tasks',
        'inputs' => [
            'clientes_id' => 'Clientes',
            'works_id' => 'Works',
            'title' => 'Title',
            'start_date' => 'Start Date',
            'end_date' => 'End Date',
            'description' => 'Description',
            'stimated_hours' => 'Stimated Hours',
            'real_hours' => 'Real Hours',
            'cost_per_hour' => 'Cost Per Hour',
        ],
    ],

    'roles' => [
        'name' => 'Roles',
        'index_title' => 'Roles List',
        'create_title' => 'Create Role',
        'edit_title' => 'Edit Role',
        'show_title' => 'Show Role',
        'inputs' => [
            'name' => 'Name',
        ],
    ],

    'permissions' => [
        'name' => 'Permissions',
        'index_title' => 'Permissions List',
        'create_title' => 'Create Permission',
        'edit_title' => 'Edit Permission',
        'show_title' => 'Show Permission',
        'inputs' => [
            'name' => 'Name',
        ],
    ],
];
