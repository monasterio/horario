@php $editing = isset($clientes) @endphp

<div class="row">
    <x-inputs.group class="col-sm-12 col-lg-7">
        <x-inputs.date
            name="registro_ciente"
            label="Registro Ciente"
            value="{{ old('registro_ciente', ($editing ? optional($clientes->registro_ciente)->format('Y-m-d') : '')) }}"
            max="255"
            required
        ></x-inputs.date>
    </x-inputs.group>

    <x-inputs.group class="col-sm-12 col-lg-4">
        <x-inputs.text
            name="name"
            label="Name"
            value="{{ old('name', ($editing ? $clientes->name : '')) }}"
            maxlength="255"
            required
        ></x-inputs.text>
    </x-inputs.group>

    <x-inputs.group class="col-sm-12">
        <x-inputs.textarea
            name="description_proyect"
            label="Description Proyect"
            maxlength="255"
            required
            >{{ old('description_proyect', ($editing ?
            $clientes->description_proyect : '')) }}</x-inputs.textarea
        >
    </x-inputs.group>
</div>
