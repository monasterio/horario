@php $editing = isset($tasks) @endphp

<div class="row">
    <x-inputs.group class="col-sm-12 col-lg-6">
        <x-inputs.select name="clientes_id" label="Clientes">
            @php $selected = old('clientes_id', ($editing ? $tasks->clientes_id : '')) @endphp
            <option disabled {{ empty($selected) ? 'selected' : '' }}>Please select the Clientes</option>
            @foreach($allClientes as $value => $label)
            <option value="{{ $value }}" {{ $selected == $value ? 'selected' : '' }} >{{ $label }}</option>
            @endforeach
        </x-inputs.select>
    </x-inputs.group>

    <x-inputs.group class="col-sm-12 col-lg-6">
        <x-inputs.select name="works_id" label="Works">
            @php $selected = old('works_id', ($editing ? $tasks->works_id : '')) @endphp
            <option disabled {{ empty($selected) ? 'selected' : '' }}>Please select the Works</option>
            @foreach($allWorks as $value => $label)
            <option value="{{ $value }}" {{ $selected == $value ? 'selected' : '' }} >{{ $label }}</option>
            @endforeach
        </x-inputs.select>
    </x-inputs.group>

    <x-inputs.group class="col-sm-12 col-lg-6">
        <x-inputs.text
            name="title"
            label="Title"
            value="{{ old('title', ($editing ? $tasks->title : '')) }}"
            maxlength="255"
            required
        ></x-inputs.text>
    </x-inputs.group>

    <x-inputs.group class="col-sm-12 col-lg-3">
        <x-inputs.datetime
            name="start_date"
            label="Start Date"
            value="{{ old('start_date', ($editing ? optional($tasks->start_date)->format('Y-m-d\TH:i:s') : '')) }}"
             min="2021-07-01T00:30" max="2021-08-30T16:30"
            required
        ></x-inputs.datetime>
    </x-inputs.group>

    <x-inputs.group class="col-sm-12 col-lg-3">
        <x-inputs.datetime
            name="end_date"
            label="End Date"
            value="{{ old('end_date', ($editing ? optional($tasks->end_date)->format('Y-m-d\TH:i:s') : '')) }}"
             min="2021-07-01T08:30" max="2021-08-30T16:30"

        required>
        </x-inputs.datetime>
    </x-inputs.group>

    <x-inputs.group class="col-sm-12">
        <x-inputs.textarea
            name="description"
            label="Description"
            maxlength="255"
            required
            >{{ old('description', ($editing ? $tasks->description : ''))
            }}</x-inputs.textarea
        >
    </x-inputs.group>

    <x-inputs.group class="col-sm-12 col-lg-4">
        <x-inputs.number
            name="stimated_hours"
            label="Stimated Hours"
             value="{{ old('stimated_hours', ($editing ? $tasks->stimated_hours : '')) }}"

            disabled="disabled"
            required
        ></x-inputs.number>
    </x-inputs.group>

    <x-inputs.group class="col-sm-12 col-lg-4">
         <x-inputs.number
            name="real_hours"
            label="Real Hours"
            value="{{ old('real_hours', ($editing ? $tasks->real_hours : '')) }}"
            max="255"
            step="0.01"
            required
        ></x-inputs.number>
    </x-inputs.group>

    <x-inputs.group class="col-sm-12 col-lg-4">
        <x-inputs.number
            name="cost_per_hour"
            label="Cost Per Hour"
            value="{{ old('cost_per_hour', ($editing ? $tasks->cost_per_hour : '')) }}"
            max="255"
            step="0.01"
            disabled="disabled"
            required
        ></x-inputs.number>
    </x-inputs.group>
</div>



