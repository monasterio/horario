@extends('layouts.app')

@section('content')
<div class="container">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title">
                <a href="{{ route('all-tasks.index') }}" class="mr-4"
                    ><i class="icon ion-md-arrow-back"></i
                ></a>
                @lang('crud.tasks.show_title')
            </h4>

            <div class="mt-4">
                <div class="mb-4">
                    <h5>@lang('crud.tasks.inputs.clientes_id')</h5>
                    <span>{{ optional($tasks->clientes)->name ?? '-' }}</span>
                </div>
                <div class="mb-4">
                    <h5>@lang('crud.tasks.inputs.works_id')</h5>
                    <span>{{ optional($tasks->works)->name ?? '-' }}</span>
                </div>
                <div class="mb-4">
                    <h5>@lang('crud.tasks.inputs.title')</h5>
                    <span>{{ $tasks->title ?? '-' }}</span>
                </div>
                <div class="mb-4">
                    <h5>@lang('crud.tasks.inputs.start_date')</h5>
                    <span>{{ $tasks->start_date ?? '-' }}</span>
                </div>
                <div class="mb-4">
                    <h5>@lang('crud.tasks.inputs.end_date')</h5>
                    <span>{{ $tasks->end_date ?? '-' }}</span>
                </div>
                <div class="mb-4">
                    <h5>@lang('crud.tasks.inputs.description')</h5>
                    <span>{{ $tasks->description ?? '-' }}</span>
                </div>
                <div class="mb-4">
                    <h5>@lang('crud.tasks.inputs.stimated_hours')</h5>
                    <span>{{ $tasks->stimated_hours ?? '-' }}</span>
                </div>
                <div class="mb-4">
                    <h5>@lang('crud.tasks.inputs.real_hours')</h5>
                    <span>{{ $tasks->real_hours ?? '-' }}</span>
                </div>
                <div class="mb-4">
                    <h5>@lang('crud.tasks.inputs.cost_per_hour')</h5>
                    <span>{{ $tasks->cost_per_hour ?? '-' }}</span>
                </div>
            </div>

            <div class="mt-4">
                <a href="{{ route('all-tasks.index') }}" class="btn btn-light">
                    <i class="icon ion-md-return-left"></i>
                    @lang('crud.common.back')
                </a>

                @can('create', App\Models\Tasks::class)
                <a href="{{ route('all-tasks.create') }}" class="btn btn-light">
                    <i class="icon ion-md-add"></i> @lang('crud.common.create')
                </a>
                @endcan
            </div>
        </div>
    </div>
</div>
@endsection
