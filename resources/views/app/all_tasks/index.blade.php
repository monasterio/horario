@extends('layouts.app')

@section('content')
<div class="container">
    <div class="card">
        <div class="card-body">
            <div style="display: flex; justify-content: space-between;">
                <h4 class="card-title">@lang('crud.tasks.index_title')</h4>
            </div>

            <div class="searchbar mt-4 mb-5">
                <div class="row">
                    <div class="col-md-6">
                        <form>
                            <div class="input-group">
                                <input
                                    id="indexSearch"
                                    type="text"
                                    name="search"
                                    placeholder="{{ __('crud.common.search') }}"
                                    value="{{ $search ?? '' }}"
                                    class="form-control"
                                    autocomplete="off"
                                />
                                <div class="input-group-append">
                                    <button
                                        type="submit"
                                        class="btn btn-primary"
                                    >
                                        <i class="icon ion-md-search"></i>
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="col-md-6 text-right">
                        @can('create', App\Models\Tasks::class)
                        <a
                            href="{{ route('all-tasks.create') }}"
                            class="btn btn-primary"
                        >
                            <i class="icon ion-md-add"></i>
                            @lang('crud.common.create')
                        </a>
                        @endcan
                    </div>
                </div>
            </div>

            <div class="table-responsive">
                <table class="table table-borderless table-hover">
                    <thead>
                        <tr>
                            <th class="text-left">
                                @lang('crud.tasks.inputs.clientes_id')
                            </th>
                            <th class="text-left">
                                @lang('crud.tasks.inputs.works_id')
                            </th>
                            <th class="text-left">
                                @lang('crud.tasks.inputs.title')
                            </th>
                            <th class="text-left">
                                @lang('crud.tasks.inputs.start_date')
                            </th>
                            <th class="text-left">
                                @lang('crud.tasks.inputs.end_date')
                            </th>
                            <th class="text-left">
                                @lang('crud.tasks.inputs.description')
                            </th>
                            <th class="text-left">
                                @lang('crud.tasks.inputs.stimated_hours')
                            </th>
                            <th class="text-right">
                                @lang('crud.tasks.inputs.real_hours')
                            </th>
                            <th class="text-right">
                                @lang('crud.tasks.inputs.cost_per_hour')
                            </th>
                            <th class="text-center">
                                @lang('crud.common.actions')
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse($allTasks as $tasks)
                        <tr>
                            <td>
                                {{ optional($tasks->clientes)->name ?? '-' }}
                            </td>
                            <td>{{ optional($tasks->works)->name ?? '-' }}</td>
                            <td>{{ $tasks->title ?? '-' }}</td>
                            <td>{{ $tasks->start_date ?? '-' }}</td>
                            <td>{{ $tasks->end_date ?? '-' }}</td>
                            <td>{{ $tasks->description ?? '-' }}</td>
                            <td>{{ $tasks->stimated_hours ?? '-' }}</td>
                            <td>{{ $tasks->real_hours ?? '-' }}</td>
                            <td>{{ $tasks->cost_per_hour ?? '-' }}</td>
                            <td class="text-center" style="width: 134px;">
                                <div
                                    role="group"
                                    aria-label="Row Actions"
                                    class="btn-group"
                                >
                                    @can('update', $tasks)
                                    <a
                                        href="{{ route('all-tasks.edit', $tasks) }}"
                                    >
                                        <button
                                            type="button"
                                            class="btn btn-light"
                                        >
                                            <i class="icon ion-md-create"></i>
                                        </button>
                                    </a>
                                    @endcan @can('view', $tasks)
                                    <a
                                        href="{{ route('all-tasks.show', $tasks) }}"
                                    >
                                        <button
                                            type="button"
                                            class="btn btn-light"
                                        >
                                            <i class="icon ion-md-eye"></i>
                                        </button>
                                    </a>
                                    @endcan @can('delete', $tasks)
                                    <form
                                        action="{{ route('all-tasks.destroy', $tasks) }}"
                                        method="POST"
                                        onsubmit="return confirm('{{ __('crud.common.are_you_sure') }}')"
                                    >
                                        @csrf @method('DELETE')
                                        <button
                                            type="submit"
                                            class="btn btn-light text-danger"
                                        >
                                            <i class="icon ion-md-trash"></i>
                                        </button>
                                    </form>
                                    @endcan
                                </div>
                            </td>
                        </tr>
                        @empty
                        <tr>
                            <td colspan="10">
                                @lang('crud.common.no_items_found')
                            </td>
                        </tr>
                        @endforelse
                    </tbody>
                    <tfoot>
                        <tr>
                            <td colspan="10">{!! $allTasks->render() !!}</td>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
