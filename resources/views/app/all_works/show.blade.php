@extends('layouts.app')

@section('content')
<div class="container">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title">
                <a href="{{ route('all-works.index') }}" class="mr-4"
                    ><i class="icon ion-md-arrow-back"></i
                ></a>
                @lang('crud.works.show_title')
            </h4>

            <div class="mt-4">
                <div class="mb-4">
                    <h5>@lang('crud.works.inputs.clientes_id')</h5>
                    <span>{{ optional($works->clientes)->name ?? '-' }}</span>
                </div>
                <div class="mb-4">
                    <h5>@lang('crud.works.inputs.name')</h5>
                    <span>{{ $works->name ?? '-' }}</span>
                </div>
                <div class="mb-4">
                    <h5>@lang('crud.works.inputs.start_date')</h5>
                    <span>{{ $works->start_date ?? '-' }}</span>
                </div>
                <div class="mb-4">
                    <h5>@lang('crud.works.inputs.end_date')</h5>
                    <span>{{ $works->end_date ?? '-' }}</span>
                </div>
                <div class="mb-4">
                    <h5>@lang('crud.works.inputs.deleted')</h5>
                    <span>{{ $works->deleted ?? '-' }}</span>
                </div>
                <div class="mb-4">
                    <h5>@lang('crud.works.inputs.description')</h5>
                    <span>{{ $works->description ?? '-' }}</span>
                </div>
            </div>

            <div class="mt-4">
                <a href="{{ route('all-works.index') }}" class="btn btn-light">
                    <i class="icon ion-md-return-left"></i>
                    @lang('crud.common.back')
                </a>

                @can('create', App\Models\Works::class)
                <a href="{{ route('all-works.create') }}" class="btn btn-light">
                    <i class="icon ion-md-add"></i> @lang('crud.common.create')
                </a>
                @endcan
            </div>
        </div>
    </div>
</div>
@endsection
